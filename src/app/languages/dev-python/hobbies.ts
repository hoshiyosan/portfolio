export const hobbies = [
    {
        label: "Musique / Séries",
        achievements: []
    },
    {
        label: "Développement / algorithmie",
        achievements: [
            "Codingame",
            "Hackathon ENSSAT",
            "Concours Meilleur dev. de France"
        ]
    },
    {
        label: "Viet vo dao (art martial)",
        achievements: [
            "Champion de France 2013",
            "Dispense de cours"
        ]
    },
    {
        label: "Maths en Jeans",
        achievements: [
            "Recherche en mathématiques",
            "Finaliste du concours André parent"
        ]
    }
]
