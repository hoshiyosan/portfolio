import { Network } from './network';

export interface Applicant {
    firstName: string;
    lastName: string;
    title: string;
    subtitle: string;
    profiles: string[];

    contact: {
        email: string;
        phoneNumber: string;

        address: {
            street: string,
            zipcode: number,
            city: string
        }
    }

    networks: Network[];
}
