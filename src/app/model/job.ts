export interface Job {
    title: string;
    company: string;
    period: string[];
    achievements: string[];
}
