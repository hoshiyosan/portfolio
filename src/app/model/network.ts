export interface Network{
    network: string;
    url: string;
    alias?: string;
    icon?: string[];
}