export interface School {
    name: string;
    diploma: string;
    description: string;


    address: string;
    zipcode: number;
    city: string;

    period: string[];
    achievements: string[];
}
