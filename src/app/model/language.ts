export interface Language {
    code?: string;
    name: string;
    level: string;
}
